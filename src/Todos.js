import React,{useState} from 'react'
import { useTodo } from "./store/TodoList"
import Lists from "./List"
import InputTodo from "./InputTodo"
import Modals from "./Modals"
function Todos() {

    const { todos } = useTodo()
    const [showModal, setShowModal] = useState(false)
    return (
        <>

            <div className="container w-10/12 mx-auto my-10">

                 <Modals show={showModal} close={()=>{setShowModal(false)}} /> 
                  <button type="button" className="px-8  my-8  text-white font-bold p-3  rounded-lg bg-indigo-400  border-t border-b border-l w-2/12 shadow-md"
                    onClick={(e) => setShowModal(true)}
                >اضافه کردن</button>
              
                <table className="table-fixed   bg-indigo-50 rounded shadow-md " >
                    <thead className="border-b-2 border-indigo-200">
                        <tr className="border-bottom">
                            <th className="w-1/2 text-right px-2 py-4">عنوان۱</th>
                            <th className="w-1/2 text-right px-2 py-4">عنوان۲ </th>
                            <th className="w-1/2 text-right px-2 py-4">عنوان۳</th>
                            <th className="w-1/2 text-center px-2 py-4">عنوان۴</th>
                        </tr>
                    </thead>

                    <tbody>


                        {todos && todos.length > 0 && todos.map((maptodo, index) => {
                            return <Lists key={maptodo.id} todo={maptodo} index={index + 1} />
                        })}
                    </tbody>
                </table>

            </div>
        </>
    )
}

export default Todos;
