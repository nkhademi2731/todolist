import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import Reducers from './Reducers';

const middlewares = [thunk];

export default createStore(Reducers, applyMiddleware(...middlewares));
