import { TODO_ADD, TODO_EDIT, TODO_DELETE } from "./Actions"

export const todoState = [
  
]


export default (state = todoState, action) => {
    switch (action.type) {
        case TODO_ADD:
            return [...state, action.payload];


        case TODO_DELETE:
            return state.filter(filtertodo => filtertodo.id !== action.payload);

        case TODO_EDIT:
            return   [action.payload,...state.filter(item => item.id !== action.payload.id)]

    }
    return state;
}



