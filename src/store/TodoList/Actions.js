import { useDispatch, useSelector } from 'react-redux';

export const TODO_ADD = "addTodo"
export const TODO_EDIT = "edittodo"
export const TODO_DELETE = "removeTodo"



export const useTodo = () => {
    const todos = useSelector((state) => state.TodoList);
    const dispatch = useDispatch();
    return {
        todos,
        dispatch,
        AddTodo,
        EditTodo,
        DeletTodo
    };
};

export const  AddTodo=(todo)=>(dispatch)=> {
    dispatch({
       type: TODO_ADD,
       payload:todo
    })
}
export const  EditTodo=(todo)=>(dispatch)=> {

    dispatch({
        type: TODO_EDIT,
        payload:todo
     })
}
export const  DeletTodo=(todoid)=>(dispatch)=> {

    dispatch({
        type:TODO_DELETE,
        payload:todoid
    })
}