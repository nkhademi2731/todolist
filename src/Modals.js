import React, { useState } from 'react'
import { v1 as uuid } from "uuid"
import { useTodo } from "./store/TodoList"
function Modals(props) {
    const { todos, dispatch, AddTodo } = useTodo();
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    if (!props.show) {
        return null;
    }
    const handleClose = () => {
        props.close();

    }

    return (
        <>
            <div className="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none" id="modal-id">
                <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">

                    <div className="">
                        <h1 onClick={handleClose}>X</h1>
                        <div className="text-center p-5 flex-auto justify-center">

                            <h2 className="text-xl font-bold py-4 ">اضافه کردن لیست علاقه مندی ها</h2>

                        </div>
                        <div className="pt-4 mx-auto">
                            <form className="my-4 ">
                                <lable>لیست ۱</lable>
                                <input

                                    onChange={(e) => setName(e.target.value)}
                                    value={name}
                                    type="text"
                                    className=" my-2 rounded-md border-t ml-0 p-2 border-b border-r text-gray-800 border-gray-200   bg-white w-full shadow-md" placeholder="" type="text" required />
                                <lable>لیست۲</lable>
                                <input

                                    onChange={(e) => setPrice(e.target.value)}
                                    value={price}
                                    type="text"
                                    className="rounded-md my-2 border-t ml-0 p-2 border-b border-r  border-gray-200 bg-white w-full  text-gray-800 shadow-md" placeholder="" type="text" required />

                                <button
                                    onClick={async () => {
                                        await dispatch(AddTodo({ id: uuid(), name: name, price: price }));
                                        setName('')
                                        setPrice('')
                                    }}
                                    type="button"
                                    className="px-8 rounded-lg  text-white font-bold p-2 my-2 uppercase bg-pink-400  border-t border-b   border-l w-full shadow-md"> اضافه کردن </button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default Modals
