import React, { useState } from 'react'
import { v1 as uuid } from "uuid"
import { useTodo } from "./store/TodoList"

function InputTodo() {
    const { todos, dispatch, AddTodo, EditTodo, DeletTodo } = useTodo();
    const [name, setName] = useState('');

    return (

        <>
            <div className="pt-4 mx-auto">
                <form className="my-4 flex ">
                    <input
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                        type="text"
                        className="rounded-r-lg p-3 border-t ml-0 border-b border-r text-gray-800 border-gray-200 bg-white w-10/12 shadow-md" placeholder="" type="text" required />
                    <button
                        onClick={async() => {
                            await dispatch(AddTodo({ id: uuid(), name: name }));
                            setName('')
                        }}
                        type="button"
                        className="px-8 rounded-l-lg  text-white font-bold p-3 uppercase bg-pink-400  border-t border-b border-l w-2/12 shadow-md">اضافه کردن</button>
                </form>
            </div>

        </>
    )
}

export default InputTodo;
