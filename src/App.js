import React, { useEffect } from 'react'
import "./App.css"
import { Provider } from 'react-redux'
import Todos from "./Todos"
import store from './store'



export default function App() {



  const stopProp = (e) => {
    e.stopPropagation();
  };

  return (
    <>

      <Provider store={store}>
        <div className="w-10/12 mx-auto">
          <Todos />
        </div>
      </Provider>

    </>
  )
}
