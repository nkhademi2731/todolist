import React, { useState } from 'react'
import { useTodo } from "./store/TodoList"




function Lists({ todo, index }) {
    const { todos, dispatch, AddTodo, EditTodo, DeletTodo } = useTodo();
    const [editTodo, setEditTodo] = useState(false);
    const [name, setName] = useState(todo.name);
    const [price, setPrice] = useState(todo.price);


    const handleDelete = () => {
        dispatch(DeletTodo(todo.id))
    }
    const handleToglleEdite = (todo) => {
        if (editTodo) {
            handleEdite(todo)
        } else {
            setEditTodo(!editTodo)
        }
    }

    const handleEdite = (todo) => {
        dispatch(EditTodo({ ...todo, name,price }))
        setEditTodo(!editTodo)
    }
    return (
        <>
            {/* <Modals  show={showTodo}  Close={setShowTodo()} yes={handleDelete}/> */}
            <tr className="px-4 border-b border-indigo-200">
                <td className="p-4 m-2 text-right" >{index}#</td>
                <div className="flex">
                    {editTodo ? <input type="text" className="rounded-lg px-3 py-2 border mt-2 text-gray-800 border-gray-200 bg-white w-10/12 shadow-md" value={name} onChange={(e) => setName(e.target.value)} type="text" required /> : <td className="p-4 m-2 mt-2 text-right" >{todo.name}</td>}
                    {editTodo ? <input type="text" className="rounded-lg px-3 py-2 border mt-2 text-gray-800 border-gray-200 bg-white w-10/12 shadow-md" value={price} onChange={(e) => setPrice(e.target.value)} type="text" required /> : <td className="p-4 m-2 mt-2 text-right" >{todo.price}</td>}
                </div>


                <td>

                    <button className="btn bg-pink-400 p-2 m-2 rounded text-white shadow-sm"
                        onClick={() => { handleToglleEdite(todo) }}>{editTodo ? "update" : "Edit"}</button>
                </td>
                <td>
                    <button className="btn bg-purple-600 p-2 m-2 rounded text-white shadow-sm"
                        onClick={() => { handleDelete() }}>Delete</button>
                </td>
            </tr>




        </>
    )
}

export default Lists
